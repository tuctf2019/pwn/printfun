# printfun

Desc: `I have made an impenetrable password checker. Just try your luck!`

Architecture: x86

Given files:

* printfun

Hints:

* Format strings can be extremely powerful.

Flag: `TUCTF{wh47'5_4_pr1n7f_l1k3_y0u_d01n6_4_b1n4ry_l1k3_7h15?}`
