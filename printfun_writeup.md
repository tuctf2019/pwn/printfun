# Printfun Author Writeup

## Description

I have made an impenetrable password checker. Just try your luck!

*Hint:* Format strings can be extremely powerful.

## A Word From The Author

printfun() is powerful and really fun to use. Under the right circumstances it can read and write arbitrary memory... Damn. Here we'll see how print() can be used to bypass a password check.

Also, I was well aware of the "brute force" method of solving this challenge before the competition. Congratulations to everyone who hates fun and probably kicks puppies. Was it wrong... not really. But the real way was so much more satisfying. The reason I consider this a lame solution is because it doesn't take much effort to get to it. CTF's are first and foremost about learning. Although this is a competition, it's like taking the easy way out. Maybe I'm being to hard on all this...

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

#define PASS_LEN 60
const int DEBUG = 1;

// Read 60 bytes of random GARBAGE
void getPass(char *pass) {
	int urandom = open("/dev/urandom", 0);
	read(urandom, pass, PASS_LEN);
	close(urandom);
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	// initialize buffers
	char *password = malloc(64);
	char *userpass = malloc(64);
	memset(password, 0, 64);
	memset(userpass, 0, 64);

	// get password
	printf("What's the password? ");
	read(0, userpass, PASS_LEN);
	getPass(password);

	// Clearly shouldn't be here
	if (DEBUG) {
		printf("__DEBUG OUTPUT__ (Disable before production)\n");
		printf("User input:\n");
		printf(userpass);
		// No brute forcing. Except y'know... people did...
		sleep(1);
	}
	
	// Passowrd check
	// strcmp() not memcmp() so it can be pwned in a bitchin' way
	if (!strcmp(userpass, password)) {
		puts("Lucky guess...");
		system("/bin/cat ./flag.txt");
	} else {
		puts("Better luck next time");
	}

    return 0;
}
```

![info.png](./res/fe1a8d4b10cf45278f41db20bcbde24f.png)

We can see 2 things:

1. The *Debug* output is still enabled so we can see our output printed back, maybe we can use that.
2. The "password" is actually just 60 bytes of random garbage.

When I can get my input printed on screen I usually try to use that to leak something either through a proximity leak or some sort of printf() format string exploit. Let's try the latter.

![format_string.png](./res/20b37ca5d035426b9339722cf24962ae.png)

And there you have it. Now, I mentioned we usually leak some sort of data using a format string (canary value, libc address, password etc.), although these all seem like dead ends. Even if we leaked a libc address, we don't have a way to take control of execution, so it does us no good, there is no canary, and the password is random everytime so leaking it wouldn't help us next run when we could input it. So what then.

printf() has more than just format strings that print information off the stack, it has one that actually **writes** to a pointer: `%n`. As it's stated:

> In C printf(), %n is a special format specifier which instead of printing something causes printf() to load the variable pointed by the corresponding argument with a value equal to the number of characters that have been printed by printf() before the occurrence of %n. - GeeksforGeeks

So if we were to print 10 bytes and then call %n, printf() would attempt to write the value *10* to first argument on the stack. Awesome! But what's on the stack?

![Screenshot from 2019-12-30 23-31-35.png](./res/cbd1572f13d84b8ca4d898687b92a13b.png)

NOTE: The password I sent was **AAAABBBB**. \
We can see our buffer is stored on the stack at the `6th` argument (+0x18) and the buffer that stores the password is stored at the `7th` argument (+0x1c).


## Exploitation

Here's the key to this challenge: we can overwrite both buffers at the same exact time. It doesn't matter what the buffers contain as long as they contain the same data. Instead of trying to match our input with what we overwrite the password buffer with, it would be smarter to overwrite both buffers with the same value. Since **0** is considered a null-byte, if we don't print anything and skip to just overwriting, we can essentially "clear" both of the buffers. Here's the syntax for fancy format strings

> %N$f

Here **N** is the argument on the stack to perform an operation on and **f** is the format string operation (p, s, d, etc.). Using this format we can manipulate just the values on the stack we want. So if we want a format string to overwrite the 6th and 7th values on the stack with null-bytes we would simply send:

`%6$n%7$n`

That's it. One of the few pwn challenges you can actually solve without a script or even a command piped to nc. Let's try it out...

![pwn.png](./res/1315dd5dccf948088b224419f54275b9.png)

--- 

For those curious, the lame solution is to understand that /dev/urandom will output any byte including the null-byte. Since I use *strcmp()* to check the passwords, if you send a blank password and the first byte of the 60 random bytes is a null-byte (1/256 chance), then you pass the check. So a quick bash script could get the flag in not long at all.

This is the command:

``` bash
# local
wile true; do ./printfun < /dev/null| grep TUCTF; done
# remote
while true; do python -c 'print "\x00"'| nc chal.tuctf.com 30501| grep TUCTF; done
```

And just wait till you see the flag get printed out. The `sleep()` was intended to deter people from trying this. But you can just run that and go make a sandwich so...

## Endgame

I really enjoyed making this challenge and hoped you had as much fun solving it. I actually stood up with excitement when I solved it myself.

> **flag:** TUCTF{wh47'5_4_pr1n7f_l1k3_y0u_d01n6_4_b1n4ry_l1k3_7h15?}

