#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

#define PASS_LEN 60
const int DEBUG = 1;

void getPass(char *pass) {
	int urandom = open("/dev/urandom", 0);
	read(urandom, pass, PASS_LEN);
	close(urandom);
}

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	char *password = malloc(64);
	char *userpass = malloc(64);
	memset(password, 0, 64);
	memset(userpass, 0, 64);

	printf("What's the password? ");
	read(0, userpass, PASS_LEN);
	getPass(password);

	if (DEBUG) {
		printf("__DEBUG OUTPUT__ (Disable before production)\n");
		printf("User input:\n");
		printf(userpass);
		// No brute forcing
		sleep(1);
	}
	
	if (!strcmp(userpass, password)) {
		puts("Lucky guess...");
		system("/bin/cat ./flag.txt");
	} else {
		puts("Better luck next time");
	}

    return 0;
}
